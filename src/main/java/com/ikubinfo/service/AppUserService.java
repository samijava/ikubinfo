package com.ikubinfo.service;

import com.ikubinfo.entity.AppUser;
import com.ikubinfo.repository.AppUserRepository;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.Optional;

@Slf4j
@Service
@RequiredArgsConstructor
public class AppUserService {

    private final AppUserRepository repository;

    private final PasswordEncoder passwordEncoder;

    @Transactional
    public AppUser save(AppUser user) {
        user.setPassword(passwordEncoder.encode(user.getPassword()));
        AppUser appUser = repository.save(user);
        log.info("User save {}" + user.getUsername());
        return appUser;
    }

    public Optional<AppUser> findByEmail(String email) {
        Optional<AppUser> user = repository.findByEmail(email);
        log.info("User Email {}" + user.map(AppUser::getEmail));
        return user;
    }

    public Optional<AppUser> findById(Long id) {
        Optional<AppUser> user = repository.findById(id);
        log.info("User ID {}" + user.map(AppUser::getUsername));
        return user;
    }

    @Transactional
    public void delete(AppUser user) {
        repository.delete(user);
        log.info("user deleted {}" + user.getUsername());
    }
}
