package com.ikubinfo.service;

import com.ikubinfo.entity.Role;
import com.ikubinfo.repository.RoleRepository;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.Optional;

@Slf4j
@Service
@RequiredArgsConstructor
public class RoleService {

    private final RoleRepository repository;

    @Transactional
    public Role save(Role role) {
        Role appUser = repository.save(role);
        log.info("Role saved {}" + role.getRoleName());
        return appUser;
    }

    public Optional<Role> findByRoleName(String roleName) {
        Optional<Role> role = repository.findByRoleName(roleName);
        log.info("Role Name {}" + role.map(Role::getRoleName));
        return role;
    }

    public Optional<Role> findById(Long id) {
        Optional<Role> role = repository.findById(id);
        log.info("Role ID {}" + role.map(Role::getId));
        return role;
    }
}
