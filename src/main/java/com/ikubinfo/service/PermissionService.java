package com.ikubinfo.service;

import com.ikubinfo.entity.Permission;
import com.ikubinfo.repository.PermissionRepository;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;
import java.util.Optional;


@Slf4j
@Service
@RequiredArgsConstructor
public class PermissionService {

    private final PermissionRepository repository;

    @Transactional
    public void saveAll(List<Permission> permissions) {
        repository.saveAll(permissions);
        log.info("permissions saved {}" + permissions);
    }

    public Optional<Permission> findById(Long id) {
        Optional<Permission> permission = repository.findById(id);
        log.info("Permission ID {}" + permission.map(Permission::getId));
        return permission;
    }

    public List<Permission> findAll() {
        List<Permission> permissions = repository.findAll();
        return permissions;
    }
}
