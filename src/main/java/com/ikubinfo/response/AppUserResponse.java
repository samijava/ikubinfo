package com.ikubinfo.response;

import com.ikubinfo.entity.AppUser;
import com.ikubinfo.entity.Role;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Set;

@Data
@NoArgsConstructor
public class AppUserResponse {

    private Long id;

    private String username;

    private String email;

    private Set<Role> roles;

    public static AppUserResponse from(AppUser appUser) {
        AppUserResponse response = new AppUserResponse();
        response.setId(appUser.getId());
        response.setUsername(appUser.getUsername());
        response.setEmail(appUser.getEmail());
        response.setRoles(appUser.getRoles());
        return response;
    }
}
