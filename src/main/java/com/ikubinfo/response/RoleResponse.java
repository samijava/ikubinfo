package com.ikubinfo.response;

import com.ikubinfo.entity.Permission;
import com.ikubinfo.entity.Role;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Set;

@Data
@NoArgsConstructor
public class RoleResponse {

    private Long id;

    private String roleName;

    private Set<Permission> permissions;

    public static RoleResponse from(Role role) {
        RoleResponse response = new RoleResponse();
        response.setId(role.getId());
        response.setRoleName(role.getRoleName());
        response.setPermissions(role.getPermissions());
        return response;
    }
}
