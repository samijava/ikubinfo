package com.ikubinfo.controller;

import com.ikubinfo.security.Login;
import com.ikubinfo.security.TokenProvider;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.json.simple.JSONObject;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;
import java.util.HashMap;
import java.util.Map;

import static com.ikubinfo.exception.ApiError.fieldError;
import static com.ikubinfo.utils.ResponseBuilder.error;
import static com.ikubinfo.utils.ResponseBuilder.success;
import static org.springframework.http.ResponseEntity.badRequest;
import static org.springframework.http.ResponseEntity.ok;

@Slf4j
@RestController
@Api(tags = "Login")
@RequiredArgsConstructor
@RequestMapping(path = "api/v1/login")
public class LoginController {

    private final AuthenticationManager authenticationManager;

    private final TokenProvider jwtProvider;

    private final UserDetailsService userDetailsService;

    @PostMapping
    @ApiOperation(value = "login through username and password", response = Object.class)
    public ResponseEntity<JSONObject> authenticationToken(
            @Valid @RequestBody Login login,
            BindingResult bindingResult) throws AuthenticationException {

        if (bindingResult.hasErrors()) {
            return badRequest().body(error(fieldError(bindingResult)).getJson());
        }

        final Authentication authentication = authenticationManager
                .authenticate(
                        new UsernamePasswordAuthenticationToken(
                                login.getUsername(),
                                login.getPassword()
                        )
                );

        SecurityContextHolder.getContext().setAuthentication(authentication);

        final UserDetails userDetails = userDetailsService.loadUserByUsername(login.getUsername());

        final String accessToken = jwtProvider.generateToken(userDetails);

        Map<String, Object> token = new HashMap<>();

        token.put("token", accessToken);
        log.info("user logged in as {} " + userDetails.getUsername());
        return ok(success(token).getJson());
    }
}
