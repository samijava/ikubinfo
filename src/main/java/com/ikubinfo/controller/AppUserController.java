package com.ikubinfo.controller;

import com.ikubinfo.dto.AppUserDto;
import com.ikubinfo.entity.AppUser;
import com.ikubinfo.entity.Role;
import com.ikubinfo.exception.ResourceNotFoundException;
import com.ikubinfo.response.AppUserResponse;
import com.ikubinfo.service.AppUserService;
import com.ikubinfo.service.RoleService;
import com.ikubinfo.validator.AppUserValidator;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import org.json.simple.JSONObject;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.validation.BindingResult;
import org.springframework.validation.ValidationUtils;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.HashSet;
import java.util.Set;

import static com.ikubinfo.exception.ApiError.fieldError;
import static com.ikubinfo.utils.ResponseBuilder.error;
import static com.ikubinfo.utils.ResponseBuilder.success;
import static org.springframework.http.ResponseEntity.badRequest;
import static org.springframework.http.ResponseEntity.ok;

@RestController
@RequiredArgsConstructor
@Api(tags = "User's data")
@RequestMapping(path = "api/v1/user")
public class AppUserController {

    private final AppUserService service;

    private final RoleService roleService;

    private final AppUserValidator validator;

    @PostMapping("/save")
    @PreAuthorize("hasAuthority('SAVE_USER')")
    @ApiOperation(value = "save user", response = AppUserResponse.class)
    public ResponseEntity<JSONObject> save(@Valid @RequestBody AppUserDto dto, BindingResult bindingResult) {

        ValidationUtils.invokeValidator(validator, dto, bindingResult);

        if (bindingResult.hasErrors()) {
            return badRequest().body(error(fieldError(bindingResult)).getJson());
        }

        AppUser user = dto.to();

        Set<Role> roles = new HashSet<>();

        dto.getRoles().forEach(role -> {
            Role r = roleService.findById(role.getId()).orElseThrow(ResourceNotFoundException::new);
            roles.add(r);
        });

        user.addRoles(roles);

        service.save(user);
        return ok(success(AppUserResponse.from(user)).getJson());
    }

    @PutMapping("/update")
    @PreAuthorize("hasAuthority('UPDATE_USER')")
    @ApiOperation(value = "update user", response = AppUserResponse.class)
    public ResponseEntity<JSONObject> update(@Valid @RequestBody AppUserDto dto, BindingResult bindingResult) {

        if (bindingResult.hasErrors()) {
            return badRequest().body(error(fieldError(bindingResult)).getJson());
        }

        AppUser user = service.findById(dto.getId()).orElseThrow(ResourceNotFoundException::new);

        user.getRoles().clear();

        Set<Role> roles = new HashSet<>();

        dto.getRoles().forEach(role -> {
            Role r = roleService.findById(role.getId()).orElseThrow(ResourceNotFoundException::new);
            roles.add(r);
        });

        user.addRoles(roles);

        dto.update(user);

        service.save(user);
        return ok(success(AppUserResponse.from(user)).getJson());
    }

    @GetMapping("/details/{userId}")
    @PreAuthorize("hasAuthority('READ_USER')")
    @ApiOperation(value = "user details by id", response = AppUserResponse.class)
    public ResponseEntity<JSONObject> update(@PathVariable Long userId) {

        AppUser user = service.findById(userId).orElseThrow(ResourceNotFoundException::new);

        return ok(success(AppUserResponse.from(user)).getJson());
    }

    @DeleteMapping("/delete/{userId}")
    @PreAuthorize("hasAuthority('DELETE_USER')")
    @ApiOperation(value = "delete user by id", response = String.class)
    public ResponseEntity<JSONObject> delete(@PathVariable Long userId) {

        AppUser user = service.findById(userId).orElseThrow(ResourceNotFoundException::new);

        service.delete(user);
        return ok(success("user successfully deleted").getJson());
    }
}
