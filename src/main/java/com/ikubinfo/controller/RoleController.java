package com.ikubinfo.controller;

import com.ikubinfo.dto.RoleDto;
import com.ikubinfo.entity.Permission;
import com.ikubinfo.entity.Role;
import com.ikubinfo.exception.ResourceNotFoundException;
import com.ikubinfo.response.RoleResponse;
import com.ikubinfo.service.PermissionService;
import com.ikubinfo.service.RoleService;
import com.ikubinfo.validator.RoleValidator;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import org.json.simple.JSONObject;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.validation.BindingResult;
import org.springframework.validation.ValidationUtils;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.HashSet;
import java.util.Set;

import static com.ikubinfo.exception.ApiError.fieldError;
import static com.ikubinfo.utils.ResponseBuilder.error;
import static com.ikubinfo.utils.ResponseBuilder.success;
import static org.springframework.http.ResponseEntity.badRequest;
import static org.springframework.http.ResponseEntity.ok;

@RestController
@RequiredArgsConstructor
@Api(tags = "Role's data")
@RequestMapping(path = "api/v1/role")
public class RoleController {

    private final RoleService service;

    private final RoleValidator validator;

    private final PermissionService permissionService;

    @PostMapping("/save")
    @PreAuthorize("hasAuthority('SAVE_ROLE')")
    @ApiOperation(value = "save role", response = RoleResponse.class)
    public ResponseEntity<JSONObject> save(@Valid @RequestBody RoleDto dto, BindingResult bindingResult) {

        ValidationUtils.invokeValidator(validator, dto, bindingResult);

        if (bindingResult.hasErrors()) {
            return badRequest().body(error(fieldError(bindingResult)).getJson());
        }

        Role role = dto.to();

        Set<Permission> permissions = new HashSet<>();

        dto.getPermissions().forEach(permission -> {
            Permission p = permissionService.findById(permission.getId())
                    .orElseThrow(ResourceNotFoundException::new);
            permissions.add(p);
        });

        role.addPermission(permissions);

        service.save(role);
        return ok(success(RoleResponse.from(role)).getJson());
    }

    @PutMapping("/update")
    @PreAuthorize("hasAuthority('UPDATE_ROLE')")
    @ApiOperation(value = "update role", response = RoleResponse.class)
    public ResponseEntity<JSONObject> update(@Valid @RequestBody RoleDto dto, BindingResult bindingResult) {

        if (bindingResult.hasErrors()) {
            return badRequest().body(error(fieldError(bindingResult)).getJson());
        }

        Role role = service.findById(dto.getId()).orElseThrow(ResourceNotFoundException::new);

        role.getPermissions().clear();

        Set<Permission> permissions = new HashSet<>();

        dto.getPermissions().forEach(permission -> {
            Permission p = permissionService.findById(permission.getId())
                    .orElseThrow(ResourceNotFoundException::new);
            permissions.add(p);
        });

        role.addPermission(permissions);

        dto.update(role);

        service.save(role);
        return ok(success(RoleResponse.from(role)).getJson());
    }

    @GetMapping("/details/{roleId}")
    @PreAuthorize("hasAuthority('READ_ROLE')")
    @ApiOperation(value = "role details by id", response = RoleResponse.class)
    public ResponseEntity<JSONObject> update(@PathVariable Long roleId) {

        Role role = service.findById(roleId).orElseThrow(ResourceNotFoundException::new);

        return ok(success(RoleResponse.from(role)).getJson());
    }
}
