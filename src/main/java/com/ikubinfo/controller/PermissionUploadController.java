package com.ikubinfo.controller;

import com.ikubinfo.dto.PermissionImportDto;
import com.ikubinfo.entity.Permission;
import com.ikubinfo.service.PermissionService;
import com.poiji.bind.Poiji;
import com.poiji.exception.PoijiExcelType;
import com.poiji.option.PoijiOptions;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import org.json.simple.JSONObject;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.List;
import java.util.stream.Collectors;

import static com.ikubinfo.utils.ResponseBuilder.success;
import static org.springframework.http.ResponseEntity.ok;

@RestController
@RequiredArgsConstructor
@Api(tags = "Permission Data Upload")
@RequestMapping(path = "api/v1/permission-data-upload")
public class PermissionUploadController {

    private final PermissionService service;

    @PostMapping("/upload")
    @ApiOperation(value = "permission data from xlsx file upload", response = Object.class)
    public ResponseEntity<JSONObject> save(@RequestPart("file") MultipartFile file) throws IOException {

        String extension = file.getOriginalFilename().substring(file.getOriginalFilename().lastIndexOf(".") + 1);

        List<PermissionImportDto> items = Poiji.fromExcel(
                file.getInputStream(),
                PoijiExcelType.valueOf(extension.toUpperCase()),
                PermissionImportDto.class,
                PoijiOptions.PoijiOptionsBuilder
                        .settings()
                        .preferNullOverDefault(true)
                        .build()
        );

        List<Permission> permissions = items.stream()
                .map(PermissionImportDto::toImportData)
                .collect(Collectors.toList());

        service.saveAll(permissions);
        return ok(success("Permission Data Uploaded").getJson());
    }

}

