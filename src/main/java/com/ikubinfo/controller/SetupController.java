package com.ikubinfo.controller;

import com.ikubinfo.entity.AppUser;
import com.ikubinfo.entity.Permission;
import com.ikubinfo.entity.Role;
import com.ikubinfo.service.AppUserService;
import com.ikubinfo.service.PermissionService;
import com.ikubinfo.service.RoleService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import org.json.simple.JSONObject;
import org.springframework.http.ResponseEntity;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import static com.ikubinfo.utils.ResponseBuilder.success;
import static org.springframework.http.ResponseEntity.ok;

@RestController
@RequiredArgsConstructor
@Api(tags = "Application Setup Data")
@RequestMapping(path = "api/v1/setup")
public class SetupController {

    private final AppUserService appUserService;

    private final PermissionService permissionService;

    private final RoleService roleService;

    private final PasswordEncoder passwordEncoder;

    @GetMapping
    @ResponseBody
    @ApiOperation(value = "application setup", response = Object.class)
    public ResponseEntity<JSONObject> setup() {
        List<Permission> permissions = permissionService.findAll();
        Set<Permission> pSets = permissions.stream().collect(Collectors.toSet());
        List<Role> roles = new ArrayList<>();
        Role role = new Role();
        role.setRoleName("ROLE_ADMIN");
        role.setPermissions(pSets);
        roleService.save(role);
        roles.add(role);
        Set<Role> sRoles = roles.stream().collect(Collectors.toSet());
        AppUser user = new AppUser();
        user.setUsername("admin");
        user.setPassword(passwordEncoder.encode("admin"));
        user.setEmail("admin@gmail.com");
        user.setEnabled(true);
        user.setRoles(sRoles);
        appUserService.save(user);
        return ok(success("Setup Done").getJson());
    }
}

