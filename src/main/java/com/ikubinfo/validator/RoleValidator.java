package com.ikubinfo.validator;

import com.ikubinfo.dto.RoleDto;
import com.ikubinfo.entity.Role;
import com.ikubinfo.service.RoleService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;

import java.util.Optional;

import static com.ikubinfo.utils.StringUtils.isNotEmpty;

@Component
@RequiredArgsConstructor
public class RoleValidator implements Validator {

    private final RoleService service;

    @Override
    public boolean supports(Class<?> clazz) {
        return RoleDto.class.isAssignableFrom(clazz);
    }

    @Override
    public void validate(Object target, Errors errors) {
        RoleDto dto = (RoleDto) target;
        if (isNotEmpty(dto.getRoleName())) {
            Optional<Role> user = service.findByRoleName(dto.getRoleName());
            if (user.isPresent()) {
                errors.rejectValue("roleName", null, "role already exist !!!");
            }
        }
    }
}
