package com.ikubinfo.validator;

import com.ikubinfo.dto.AppUserDto;
import com.ikubinfo.entity.AppUser;
import com.ikubinfo.service.AppUserService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;

import java.util.Optional;

import static com.ikubinfo.utils.StringUtils.isNotEmpty;

@Component
@RequiredArgsConstructor
public class AppUserValidator implements Validator {

    private final AppUserService service;

    @Override
    public boolean supports(Class<?> clazz) {
        return AppUserDto.class.isAssignableFrom(clazz);
    }

    @Override
    public void validate(Object target, Errors errors) {
        AppUserDto dto = (AppUserDto) target;
        if (isNotEmpty(dto.getEmail())) {
            Optional<AppUser> user = service.findByEmail(dto.getEmail());
            if (user.isPresent()) {
                errors.rejectValue("email", null, "email already exist !!!");
            }
        }
    }
}
