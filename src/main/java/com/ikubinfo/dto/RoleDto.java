package com.ikubinfo.dto;

import com.ikubinfo.entity.Permission;
import com.ikubinfo.entity.Role;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Set;

@Data
@NoArgsConstructor
public class RoleDto {

    private Long id;

    private String roleName;

    private Set<Permission> permissions;

    public Role to() {
        Role role = new Role();
        role.setRoleName(roleName);
        return role;
    }

    public void update(Role role) {
        role.setRoleName(roleName);
        role.setPermissions(permissions);
    }
}
