package com.ikubinfo.dto;

import com.ikubinfo.entity.AppUser;
import com.ikubinfo.profile.RoleProfile;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Set;

@Data
@NoArgsConstructor
public class AppUserDto {

    private Long id;

    private String username;

    private String password;

    private String email;

    private Set<RoleProfile> roles;

    public AppUser to(){
        AppUser user = new AppUser();
        user.setUsername(username);
        user.setEmail(email);
        user.setEnabled(true);
        return user;
    }

    public void update(AppUser user){
        user.setUsername(username);
        user.setEmail(email);
        user.setEnabled(true);
    }
}
