package com.ikubinfo.entity;

import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.DynamicUpdate;

import javax.persistence.*;
import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

@Data
@Entity
@DynamicUpdate
@NoArgsConstructor
@Table(name = "APP_USER")
public class AppUser implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "USER_ID")
    private Long id;

    @Column(name ="USER_NAME" ,length = 30)
    private String username;

    @Column(name = "PASSWORD")
    private String password;

    @Column(name ="EMAIL" ,length = 50)
    private String email;

    @ManyToMany(
            cascade = CascadeType.ALL,
            fetch = FetchType.EAGER
    )
    @JoinTable(
            name = "APPUSER_ROLE",
            joinColumns = @JoinColumn(name = "USER_ID"),
            inverseJoinColumns = @JoinColumn(name = "ROLE_ID")
    )
    private Set<Role> roles;

    @Column(name = "ENABLED")
    private Boolean enabled;

    public void addRoles(Set<Role> roleSet) {
        if (roles == null) {
            roles = new HashSet<>();
        }
        roles.addAll(roleSet);
    }
}
