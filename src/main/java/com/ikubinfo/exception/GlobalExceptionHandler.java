package com.ikubinfo.exception;

import com.ikubinfo.utils.ResponseBuilder;
import lombok.extern.slf4j.Slf4j;
import org.json.simple.JSONObject;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

@Slf4j
@ControllerAdvice(basePackages = "com.ikubinfo.controller")
public class GlobalExceptionHandler extends ResponseEntityExceptionHandler {


    @ExceptionHandler({Exception.class})
    public ResponseEntity<JSONObject> handleGlobalExceptions(Exception ex, WebRequest request) {
        return new ResponseEntity<>(ResponseBuilder.error(("INTERNAL SERVER ERROR")).getJson(), HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler({ResourceNotFoundException.class})
    public ResponseEntity<JSONObject> handleResourceNotFoundExceptions(Exception ex, WebRequest request) {
        return new ResponseEntity<>(ResponseBuilder.error(("RESOURCE NOT FOUND")).getJson(), HttpStatus.NOT_FOUND);
    }
}


