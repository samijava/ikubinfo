package com.ikubinfo.security;

import com.ikubinfo.entity.AppUser;
import com.ikubinfo.entity.Permission;
import com.ikubinfo.entity.Role;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

public final class AuthUserFactory {

    private AuthUserFactory() {
    }

    public static AuthUser create(AppUser user) {
        return new AuthUser(user.getId(), user.getUsername(), user.getEmail(),
                user.getPassword(), getAuthorities(user.getRoles()), user.getEnabled());
    }

    private static Collection<? extends GrantedAuthority> getAuthorities(Collection<Role> roles) {
        return getGrantedAuthorities(getPrivileges(roles));
    }

    private static List<String> getPrivileges(Collection<Role> roles) {
        List<String> privileges = new ArrayList<>();
        List<Permission> collection = new ArrayList<>();
        roles.forEach(role -> {
            collection.addAll(role.getPermissions());
        });
        collection.forEach(item -> {
            privileges.add(item.getAuthority());
        });
        return privileges;
    }

    private static List<GrantedAuthority> getGrantedAuthorities(List<String> privileges) {
        List<GrantedAuthority> authorities = new ArrayList<>();
        privileges.forEach(privilege -> {
            authorities.add(new SimpleGrantedAuthority(privilege));
        });
        return authorities;
    }

}

