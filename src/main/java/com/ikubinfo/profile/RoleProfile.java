package com.ikubinfo.profile;

import com.ikubinfo.entity.Role;
import lombok.Data;

@Data
public class RoleProfile {

    private Long id;

    private String roleName;

    public Role to(){
        Role role = new Role();
        role.setRoleName(roleName);
        return role;
    }
}
