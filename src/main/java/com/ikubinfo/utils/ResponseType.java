package com.ikubinfo.utils;

public enum ResponseType {

    DATA,
    ERROR;
}