1. go to http://localhost:8000/swagger-ui/ and Browse first 
Permission Data Upload xlsx file which is located in data directory in project.
2. Then go to Application Setup Data where you can set user and role as admin.
3. SQL Server Database user name : sa and password: arafinN123456 but you can change it in application.yml file
4. You can find swagger-ui, to login request. In login set user name and password as admin and after that a token will be generated. After getting token, put the token the top right corner as Authorize Tab, and put token as    Bearer token.
5. Database : MSSQL Server
6. IDE: Intellij Idea. Please install lombok plugin in your IDE
7. In this project, I have used gradle-7.1.1. You need to install gradle-7.1.1
8. JDK: I have user OpenJDK-15
9. I have used OPEN-API 3 , so you do not need to use Rest Client Tools. So you can use swagger and can browse API faster.

Project Setup
1. First Create Permissions
2. Then Create Role and assign permissions to Role for example if I have permissions ID as 1,2,3 
{
   "permissions": [
   {
   "id": 1
   },
   {
   "id": 2
   },
   {
   "id": 3
   }
   ],
   "roleName": "ROLE_USER"
 }
3. After that create user and while creating user assign role or roles to user as 
 {
   "email": "sami@gmail.com",
   "password": "password",
   "roles": [
   {
   "id": 1
   }
   ],
   "username": "sami"
}